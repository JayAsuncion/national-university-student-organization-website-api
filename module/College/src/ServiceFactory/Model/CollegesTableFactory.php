<?php
namespace College\ServiceFactory\Model;

use College\Model\College;
use College\Model\CollegesTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class CollegesTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new College());

        $tableGateway = new TableGateway(
            'colleges',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new CollegesTable($tableGateway);
    }
}

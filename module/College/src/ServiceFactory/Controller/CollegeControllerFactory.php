<?php
namespace College\ServiceFactory\Controller;

use Auth\Service\AuthService;
use College\Controller\CollegeController;
use College\Model\CollegesTable;
use Psr\Container\ContainerInterface;

class CollegeControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $collegesTable = $container->get(CollegesTable::class);

        return new CollegeController(
            $authService,
            $collegesTable
        );
    }
}

<?php
namespace College\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use College\Model\CollegesTable;
use Zend\View\Model\JsonModel;

class CollegeController extends AppAbstractRestfulController
{
    private $collegesTable;

    public function __construct(
        AuthService $authService,
        CollegesTable $collegesTable
    ) {
        parent::__construct($authService);
        $this->collegesTable = $collegesTable;
    }

    public function getList()
    {
        $isCollege = $this->params()->fromQuery('is_college');
        $collegeListResultSet = $this->collegesTable->getCollegeList($isCollege);
        $collegeList = iterator_to_array($collegeListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'colleges' => $collegeList
            ]
        ]);
    }

    public function get($id)
    {
        $collegeID = $id;
        $collegeResultSet = $this->collegesTable->getCollegeByCollegeID($collegeID);
        $college = $collegeResultSet->getDataSource()->current();

        if (empty($college)) {
            return new JsonModel([
                'success' => false,
                'message' => 'College #' . $id . ' not found.'
            ]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'college' => $college
            ]
        ]);
    }

    public function create($data)
    {
        $collegeData = [];

        if (!empty($data['college_name'])) $collegeData['college_name'] = $data['college_name'];
        if (!empty($data['college_description'])) $collegeData['college_description'] = $data['college_description'];
        if (!empty($data['college_logo'])) $collegeData['college_logo'] = json_encode($data['college_logo']);
        if (!empty($data['college_courses_offered'])) $collegeData['college_courses_offered'] = json_encode($data['college_courses_offered']);
        if (!empty($data['college_facebook_url'])) $collegeData['college_facebook_url'] = $data['college_facebook_url'];

        $collegeID = $this->collegesTable->insertCollege($collegeData);

        return new JsonModel([
            'success' => true,
            'data' => [
                'college_id' => $collegeID
            ]
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['delete_flag'])) {
            $collegeData = ['delete_flag' => 'y'];
            $this->collegesTable->updateCollege($id, $collegeData);

            return new JsonModel([
                'success' => true,
                'message' => 'College #' . $id . ' delete successful'
            ]);
        } else {
            $collegeData = [];

            if (!empty($data['college_name'])) $collegeData['college_name'] = $data['college_name'];
            if (!empty($data['college_description'])) $collegeData['college_description'] = $data['college_description'];
            if (!empty($data['college_logo'])) $collegeData['college_logo'] = json_encode($data['college_logo']);
            if (!empty($data['college_courses_offered'])) $collegeData['college_courses_offered'] = json_encode($data['college_courses_offered']);
            if (!empty($data['college_facebook_url'])) $collegeData['college_facebook_url'] = $data['college_facebook_url'];

            $collegeID = $id;
            $affectedRow = $this->collegesTable->updateCollege($collegeID, $collegeData);

            if (empty($affectedRow)) {
                return $this->resourceNotFoundError();
            }

            return new JsonModel([
                'success' => true,
                'data' => [
                    'college_id' => $collegeID
                ]
            ]);
        }
    }
}

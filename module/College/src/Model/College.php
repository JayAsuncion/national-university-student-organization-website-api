<?php
namespace College\Model;

class College
{
    public $college_id;
    public $college_name;
    public $college_logo;
    public $is_college;

    public function exchangeArray($data)
    {
        $this->college_id = !empty($data['college_id']) ? $data['college_id'] : '';
        $this->college_name = !empty($data['college_name']) ? $data['college_name'] : '';
        $this->college_logo = !empty($data['college_logo']) ? $data['college_logo'] : '';
        $this->is_college = !empty($data['is_college']) ? $data['is_college'] : '';
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }
}

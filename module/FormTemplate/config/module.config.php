<?php
namespace FormTemplate;

use FormTemplate\Controller\FormTemplateController;
use FormTemplate\Model\FormTemplateContentsTable;
use FormTemplate\Model\FormTemplateSchemaTable;
use FormTemplate\ServiceFactory\Controller\FormTemplateSchemaControllerFactory;
use FormTemplate\ServiceFactory\Model\FormTemplateContentsTableFactory;
use FormTemplate\ServiceFactory\Model\FormTemplateSchemaTableFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => array(
        'routes' => array (
            'form-template' => array(
                'type' => Segment::class,
                'options' => array(
                    'route' =>'/form-templates[/:id]',
                    'defaults' => array(
                        'controller' => FormTemplateController::class
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'factories' => array(
            FormTemplateController::class => FormTemplateSchemaControllerFactory::class
        )
    ),
    'service_manager' => array(
        'factories' => array(
            FormTemplateContentsTable::class => FormTemplateContentsTableFactory::class,
            FormTemplateSchemaTable::class => FormTemplateSchemaTableFactory::class
        ),
        'invokables' => array(

        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);

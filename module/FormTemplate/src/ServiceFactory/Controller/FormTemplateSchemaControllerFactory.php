<?php
namespace FormTemplate\ServiceFactory\Controller;

use Auth\Service\AuthService;
use FormTemplate\Controller\FormTemplateController;
use FormTemplate\Model\FormTemplateSchemaTable;
use Psr\Container\ContainerInterface;

class FormTemplateSchemaControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $formTemplateSchemaTable = $container->get(FormTemplateSchemaTable::class);
        $authService = $container->get(AuthService::class);

        return new FormTemplateController(
            $formTemplateSchemaTable,
            $authService
        );
    }
}

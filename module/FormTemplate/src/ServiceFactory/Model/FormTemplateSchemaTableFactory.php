<?php
namespace FormTemplate\ServiceFactory\Model;

use FormTemplate\Model\FormTemplateSchema;
use FormTemplate\Model\FormTemplateSchemaTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class FormTemplateSchemaTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new FormTemplateSchema());

        $tableGateway = new TableGateway(
            'form_template_schema',
            $dbAdapter,
            null,
            $resultSetPrototype
        );
        return new FormTemplateSchemaTable($tableGateway);
    }
}

<?php
namespace FormTemplate\ServiceFactory\Model;

use FormTemplate\Model\FormTemplateContent;
use FormTemplate\Model\FormTemplateContentsTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class FormTemplateContentsTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new FormTemplateContent());

        $tableGateway = new TableGateway(
            'form_template_contents',
            $dbAdapter,
            null,
            $resultSetPrototype
        );
        return new FormTemplateContentsTable($tableGateway);
    }
}

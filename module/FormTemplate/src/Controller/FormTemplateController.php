<?php
namespace FormTemplate\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use FormTemplate\Model\FormTemplateSchemaTable;
use Zend\View\Model\JsonModel;

class FormTemplateController extends AppAbstractRestfulController
{
    private $formTemplateSchemaTable;

    public function __construct(
        FormTemplateSchemaTable $formTemplateSchemaTable,
        AuthService $authService
    ) {
        parent::__construct($authService);
        $this->formTemplateSchemaTable = $formTemplateSchemaTable;
    }

    public function getList()
    {
        $queryParams = $this->params()->fromQuery();
        $columnParam = $queryParams['column'];
        $formTemplateSchemaResultSet = '';

        switch ($columnParam) {
            case 'template_name':
                $formTemplateSchemaResultSet = $this->formTemplateSchemaTable->fetchAllFormTemplateSchemaCodesAndName();
                break;
            default:
                $formTemplateSchemaResultSet = $this->formTemplateSchemaTable->fetchAllFormTemplateSchema();
        }

        $formTemplateSchemaList = iterator_to_array($formTemplateSchemaResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'form_template_schema_list' => $formTemplateSchemaList
            ]
        ]);
    }

    public function get($templateCode)
    {
        $formTemplateSchemaResultSet = $this->formTemplateSchemaTable->fetchFormTemplateSchema($templateCode);
        $formTemplateSchema = $formTemplateSchemaResultSet->getDataSource()->current();
        return new JsonModel([
            'success' => true,
            'data' => [
                'schema' => $formTemplateSchema['schema']
            ]
        ]);
    }

    public function options()
    {
        $response = $this->getResponse();

        // if in Options array, Allow
        $response->getHeaders()->addHeaderLine('Access-Control-Allow-Methods', implode(',', $this->_getOptions()));

        return $response;
    }
}

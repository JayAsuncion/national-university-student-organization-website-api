<?php
namespace FormTemplate\Model;


class FormTemplateContent
{
    public $form_template_content_id;
    public $json_content;
    public $template_code;

    public function exchangeArray($data)
    {
        $this->form_template_content_id = (!empty($data['form_template_content_id'])) ? $data['form_template_content_id'] : 0;
        $this->json_content = (!empty($data['json_content'])) ? $data['json_content'] : '';
        $this->template_code = (!empty($data['template_code'])) ? $data['template_code'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

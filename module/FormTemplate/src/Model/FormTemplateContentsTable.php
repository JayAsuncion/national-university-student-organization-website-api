<?php
namespace FormTemplate\Model;

use Zend\Db\TableGateway\TableGateway;

class FormTemplateContentsTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchFormTemplateContent($formTemplateContentID) {
        $select = $this->tableGateway->getSql()->select();
        $select->where(['form_template_content_id' => $formTemplateContentID]);
        return $this->tableGateway->selectWith($select);
    }

    public function insertFormTemplateContent($data) {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateFormTemplateContent($formTemplateContentID, $data) {
        $this->tableGateway->update($data, ['form_template_content_id' => $formTemplateContentID]);
    }
}

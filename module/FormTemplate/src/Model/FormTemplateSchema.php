<?php
namespace FormTemplate\Model;

class FormTemplateSchema
{
    public $template_code;
    public $template_name;
    public $schema;

    public function exchangeArray($data)
    {
        $this->template_code = (!empty($data['template_code'])) ? $data['template_code'] : '';
        $this->template_name = (!empty($data['template_name'])) ? $data['template_name'] : '';
        $this->schema = (!empty($data['schema'])) ? $data['schema'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

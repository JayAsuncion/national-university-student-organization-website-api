<?php
namespace FormTemplate\Model;

use Zend\Db\TableGateway\TableGateway;

class FormTemplateSchemaTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchFormTemplateSchema($templateCode)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->where([
            'template_code' => $templateCode
        ]);
        return $resultSet = $this->tableGateway->selectWith($select);
    }

    public function fetchAllFormTemplateSchema()
    {
        $select = $this->tableGateway->getSql()->select();
        return $this->tableGateway->selectWith($select);
    }

    public function fetchAllFormTemplateSchemaCodesAndName()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['template_code', 'template_name']);
        return $this->tableGateway->selectWith($select);
    }
}

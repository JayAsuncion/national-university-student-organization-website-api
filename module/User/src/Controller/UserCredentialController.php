<?php
namespace User\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use User\Model\UserCollegeTable;
use User\Model\UserCredentialTable;
use User\Model\UserInfoTable;
use Zend\View\Model\JsonModel;

class UserCredentialController extends AppAbstractRestfulController
{
    protected $userCredentialTable;
    protected $userCollegeTable;
    protected $userInfoTable;

    public function __construct(
        AuthService $authService,
        UserCredentialTable $userCredentialTable,
        UserCollegeTable $userCollegeTable,
        UserInfoTable $userInfoTable
    ) {
        parent::__construct($authService);
        $this->userCredentialTable = $userCredentialTable;
        $this->userCollegeTable = $userCollegeTable;
        $this->userInfoTable = $userInfoTable;
    }

    public function getList()
    {
        $userListResultSet = $this->userCredentialTable->getUserList();
        $userList = iterator_to_array($userListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'user_list' => $userList
            ]
        ]);
    }

    public function get($id)
    {
        $userResultSet = $this->userCredentialTable->getUserByUserCredentialID($id);
        $user = $userResultSet->getDataSource()->current();

        if (empty($user)) {
            return new JsonModel([
                'success' => false,
                'message' => 'User #'. $id . ' not found.'
            ]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'user' => $user
            ]
        ]);
    }

    public function create($data)
    {
        $userInfoData = [
            'first_name' => !empty($data['first_name']) ? $data['first_name'] : '',
            'middle_name' => !empty($data['middle_name']) ? $data['middle_name'] : '',
            'last_name' => !empty($data['last_name']) ? $data['last_name'] : ''
        ];
        $userInfoID = $this->userInfoTable->insertUserInfo($userInfoData);

        $userCredentialData = [
            'email' => !empty($data['email']) ? $data['email'] : '',
            'password' => !empty($data['password']) ? $data['password'] : '',
            'user_credential_name' => !empty($data['user_credential_name']) ? $data['user_credential_name'] : '',
            'user_type_id' => !empty($data['user_type_id']) ? $data['user_type_id'] : '',
            'user_info_id' => $userInfoID,
            'active_flag' => 'y'
        ];
        $userCredentialID = $this->userCredentialTable->insertUser($userCredentialData);

        $userCollegeData = [
            'user_credential_id' => $userCredentialID,
            'college_id' => $data['college_id']
        ];
        $this->userCollegeTable->insertUserCollege($userCollegeData);

        return new JsonModel([
            'success' => true,
            'data' => [
                'user_credential_id' => $userCredentialID
            ]
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['active_flag'])) {
            $userCredentialID = $id;
            $userCredentialData = [
                'active_flag' => 'n'
            ];

            $this->userCredentialTable->updateUser($userCredentialData, $userCredentialID);

            return new JsonModel([
                'success' => false,
                'message' => 'User #' . $userCredentialID . ' delete successful.'
            ]);

        } else {
            $userCredentialID = $id;
            $userCredentialData = [
                'email' => !empty($data['email']) ? $data['email'] : '',
                'password' => !empty($data['password']) ? $data['password'] : '',
                'user_credential_name' => !empty($data['user_credential_name']) ? $data['user_credential_name'] : '',
                'user_type_id' => !empty($data['user_type_id']) ? $data['user_type_id'] : '',
                'active_flag' => 'y'
            ];
            $this->userCredentialTable->updateUser($userCredentialData, $userCredentialID);

            $userInfoID = $this->userCredentialTable->getUserInfoIDByUserCredentialID($userCredentialID);
            $userInfoData = [
                'first_name' => !empty($data['first_name']) ? $data['first_name'] : '',
                'middle_name' => !empty($data['middle_name']) ? $data['middle_name'] : '',
                'last_name' => !empty($data['last_name']) ? $data['last_name'] : ''
            ];
            $this->userInfoTable->updateUserInfo($userInfoData, $userInfoID);

            $this->userCollegeTable->deleteUserCollegeByUserCredentialID($userCredentialID);
            $userCollegeData = [
                'user_credential_id' => $userCredentialID,
                'college_id' => $data['college_id']
            ];
            $this->userCollegeTable->insertUserCollege($userCollegeData);

            return new JsonModel([
                'success' => true,
                'data' => [
                    'user_credential_id' => $userCredentialID
                ]
            ]);
        }
    }
}

<?php
namespace User\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use User\Model\UserTypeModuleTable;
use User\Model\UserTypeTable;
use Zend\View\Model\JsonModel;

class UserTypeController extends AppAbstractRestfulController
{
    protected $userTypeTable;
    protected $userTypeModuleTable;

    public function __construct(
        AuthService $authService,
        UserTypeTable $userTypeTable,
        UserTypeModuleTable $userTypeModuleTable
    ) {
        parent::__construct($authService);
        $this->userTypeTable = $userTypeTable;
        $this->userTypeModuleTable = $userTypeModuleTable;
    }

    public function getList()
    {
        $userTypeResultSet = $this->userTypeTable->getUserTypeList();
        $userTypes = iterator_to_array($userTypeResultSet->getDataSource());

        $formattedUserTypes = [];


        foreach ($userTypes as $userType) {
            $modulesResultSet = $this->userTypeModuleTable->getModuleCodesByUserTypeID($userType['user_type_id']);
            $modules = iterator_to_array($modulesResultSet->getDataSource());
            $userTypeModules = [];

            foreach ($modules as $module) {
                $userTypeModules[] = $module['module_label'];
            }

            if (empty($formattedUserTypes[$userType['user_type_id']])) {
                $formattedUserTypes[] = [
                    'user_type_id' => $userType['user_type_id'],
                    'user_type_label' => $userType['user_type_label'],
                    'modules' => $userTypeModules
                ];
            }
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'user_type_list' => $formattedUserTypes
            ]
        ]);
    }

    public function get($id)
    {
        $userTypeResultSet = $this->userTypeTable->getUserTypeByUserTypeID($id);
        $userTypeAndModules = iterator_to_array($userTypeResultSet->getDataSource());

        if (empty($userTypeAndModules)) {
            return new JsonModel([
                'success' => false,
                'message' => 'User Type #' . $id . ' not found.'
            ]);
        }

        $formattedUserType = [];

        foreach ($userTypeAndModules as $item) {
            if (empty($formattedUserType[$item['user_type_id']])) {
                $formattedUserType[$item['user_type_id']] = [
                    'user_type_id' => $item['user_type_id'],
                    'user_type_label' => $item['user_type_label']
                ];
            }

            if (!empty($item['module_code'])) {
                $formattedUserType[$item['user_type_id']]['modules'][] = $item['module_code'];
            }
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'user_type' => $formattedUserType
            ]
        ]);
    }

    public function create($data)
    {
        $userTypeFormData = [
            'user_type_label' => !empty($data['user_type_label']) ? $data['user_type_label'] : ''
        ];
        $userTypeID = $this->userTypeTable->insertUserType($userTypeFormData);
        $this->userTypeModuleTable->deleteUserTypeModuleByUserTypeID($userTypeID);

        $modules = $data['modules'];

        foreach($modules as $module) {
            $this->userTypeModuleTable->insertUserTypeModule([
                'user_type_id' => $userTypeID,
                'module_code' => $module
            ]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'user_type_id' => $userTypeID
            ]
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['delete_flag'])) {
            $userTypeID = $id;
            $userTypeData = ['delete_flag' => 'y'];

            $this->userTypeTable->updateUserType($userTypeID, $userTypeData);

            return new JsonModel([
                'success' => true,
                'message' => 'User Type #' . $id . ' delete successful'
            ]);
        } else {
            $userTypeID = $id;
            $modules = $data['modules'];

            $userTypeData = [
                'user_type_label' => !empty($data['user_type_label']) ?  $data['user_type_label'] : ''
            ];

            $this->userTypeTable->updateUserType($userTypeID, $userTypeData);

            $this->userTypeModuleTable->deleteUserTypeModuleByUserTypeID($userTypeID);

            foreach($modules as $module) {
                $this->userTypeModuleTable->insertUserTypeModule([
                    'user_type_id' => $userTypeID,
                    'module_code' => $module
                ]);
            }

            return new JsonModel([
                'success' => true,
                'data' => [
                    'user_type_id' => $userTypeID
                ]
            ]);
        }
    }
}

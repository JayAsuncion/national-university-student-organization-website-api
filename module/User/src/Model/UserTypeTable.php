<?php
namespace User\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class UserTypeTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getUserTypeList()
    {
        return $this->tableGateway->select(['sms_flag' => 'y', 'delete_flag' => 'n']);
    }

    public function getUserTypeByUserTypeID($userTypeID)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'user_type_id', 'user_type_label',
            'module_code' => new Expression('utm.module_code')
        ])->join(
            ['utm' => 'user_types_modules'], 'user_types.user_type_id = utm.user_type_id', [], 'LEFT'
        )->where([
            'user_types.user_type_id' => $userTypeID,
            'delete_flag' => 'n'
        ]);

        return $this->tableGateway->selectWith($select);
    }

    public function insertUserType($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateUserType($userTypeID, $data)
    {
        return $this->tableGateway->update($data, ['user_type_id' => $userTypeID]);
    }
}

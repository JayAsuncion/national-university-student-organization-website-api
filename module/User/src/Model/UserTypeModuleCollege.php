<?php
namespace User\Model;

class UserTypeModuleCollege
{
    public $user_type_module_college_id;
    public $user_type_module_id;
    public $college_id;

    public function exchangeArray($data)
    {
        $this->user_type_module_college_id = (!empty($data['user_type_module_college_id'])) ? $data['user_type_module_college_id'] : '';
        $this->user_type_module_id = (!empty($data['user_type_module_id'])) ? $data['user_type_module_id'] : '';
        $this->college_id = (!empty($data['college_id'])) ? $data['college_id'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

<?php
namespace User\Model;

class UserType
{
    public $user_type_id;
    public $user_type_label;
    public $sms_flag;

    public function exchangeArray($data)
    {
        $this->user_type_id = (!empty($data['user_type_id'])) ? $data['user_type_id'] : 0;
        $this->user_type_label = (!empty($data['user_type_label'])) ? $data['user_type_label'] : '';
        $this->sms_flag = (!empty($data['sms_flag'])) ? $data['sms_flag'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

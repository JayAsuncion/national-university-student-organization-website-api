<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class UserCollegeTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function insertUserCollege($data)
    {
        $this->tableGateway->insert($data);
    }

    public function deleteUserCollegeByUserCredentialID($userCredentialID)
    {
        return $this->tableGateway->delete(['user_credential_id' => $userCredentialID]);
    }
}

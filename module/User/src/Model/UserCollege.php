<?php
namespace User\Model;

class UserCollege
{
    public $user_college_id;
    public $user_credential_id;
    public $college_id;

    public function exchangeArray($data)
    {
        $this->user_college_id = (!empty($data['user_college_id'])) ? $data['user_college_id'] : 0;
        $this->user_credential_id = (!empty($data['user_credential_id'])) ? $data['user_credential_id'] : 0;
        $this->college_id = (!empty($data['college_id'])) ? $data['college_id'] : 0;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

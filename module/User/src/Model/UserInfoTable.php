<?php
namespace User\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class UserInfoTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function insertUserInfo($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateUserInfo($data, $userInfoID)
    {
        return $this->tableGateway->update($data, ['user_info_id' => $userInfoID]);
    }
}

<?php
namespace User\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class UserTypeModuleTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchUserTypeModulesUsingUserTypeID($userTypeID) {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'module_code'
        ])->where(['user_type_id' => $userTypeID]);
        return $this->tableGateway->selectWith($select);
    }

    public function getModuleCodesByUserTypeID($userTypeID)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'module_label' => new Expression('m.module_label')
        ] )->join(
            ['m' => 'modules'], 'user_types_modules.module_code = m.module_code', [], 'INNER'
        )->where(['user_type_id' => $userTypeID]);
        return $this->tableGateway->selectWith($select);
    }

    public function insertUserTypeModule($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function deleteUserTypeModuleByUserTypeID($userTypeID)
    {
        return $this->tableGateway->delete(['user_type_id' => $userTypeID]);
    }
}

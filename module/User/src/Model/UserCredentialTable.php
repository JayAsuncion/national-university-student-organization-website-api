<?php
namespace User\Model;


use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class UserCredentialTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchUserCredentialsByEmailAndPassword($email, $password)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'user_credential_id', 'email','user_credential_name', 'user_type_id', 'user_info_id', 'active_flag',
            'user_type_label' => new Expression('ut.user_type_label'),
            'college_id' => new Expression('uc.college_id')
        ]);
        $select->where(['email' => $email, 'password' => $password, 'delete_flag' => 'n']);
        $select->join(['ut' => 'user_types'], 'users_credentials.user_type_id = ut.user_type_id', [], 'INNER');
        $select->join(['uc' => 'users_colleges'], 'users_credentials.user_credential_id = uc.user_credential_id', [], 'INNER');

        return $this->tableGateway->selectWith($select);
    }

    public function getUserList()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'user_credential_id', 'email', 'user_credential_name',
            'full_name' => new Expression('CONCAT(ui.first_name, " ", ui.last_name)'),
            'user_type_label' => new Expression('ut.user_type_label')
        ])->join(
            ['ui' => 'users_info'], 'users_credentials.user_info_id = ui.user_info_id', [], 'INNER'
        )->join(
            ['ut' => 'user_types'], 'users_credentials.user_type_id = ut.user_type_id', [], 'INNER'
        );
        $select->where(['active_flag' => 'y', 'users_credentials.sms_flag' => 'y']);
        return $this->tableGateway->selectWith($select);
    }

    public function getUserByUserCredentialID($userCredentialID)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'user_credential_id', 'email', 'password', 'user_credential_name', 'user_type_id',
            'first_name' => new Expression('ui.first_name'),
            'middle_name' => new Expression('ui.middle_name'),
            'last_name' => new Expression('ui.last_name'),
            'college_id' => new Expression('uc.college_id'),
        ])->join(
            ['ui' => 'users_info'], 'users_credentials.user_info_id = ui.user_info_id', [], 'INNER'
        )->join(
            ['uc' => 'users_colleges'], 'users_credentials.user_credential_id = uc.user_credential_id', [], 'INNER'
        )->where(['users_credentials.user_credential_id' => $userCredentialID, 'active_flag' => 'y', 'users_credentials.sms_flag' => 'y']);
        return $this->tableGateway->selectWith($select);
    }

    public function getUserInfoIDByUserCredentialID($userCredentialID)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'user_info_id'
        ])->where(['user_credential_id' => $userCredentialID]);
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet->getDataSource()->current();
    }

    public function insertUser($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateUser($data, $userCredentialID)
    {
        return $this->tableGateway->update($data, ['user_credential_id' => $userCredentialID]);
    }
}

<?php
namespace User\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Psr\Container\ContainerInterface;
use User\Controller\UserTypeController;
use User\Model\UserTypeModuleTable;
use User\Model\UserTypeTable;

class UserTypeControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $userTypeTable = $container->get(UserTypeTable::class);
        $userTypeModuleTable = $container->get(UserTypeModuleTable::class);

        return new UserTypeController(
            $authService,
            $userTypeTable,
            $userTypeModuleTable
        );
    }
}

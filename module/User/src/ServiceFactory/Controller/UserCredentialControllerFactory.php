<?php
namespace User\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Psr\Container\ContainerInterface;
use User\Controller\UserCredentialController;
use User\Model\UserCollegeTable;
use User\Model\UserCredentialTable;
use User\Model\UserInfoTable;

class UserCredentialControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $userCredentialTable = $container->get(UserCredentialTable::class);
        $userCollegeTable = $container->get(UserCollegeTable::class);
        $userInfoTable = $container->get(UserInfoTable::class);

        return new UserCredentialController(
            $authService,
            $userCredentialTable,
            $userCollegeTable,
            $userInfoTable
        );
    }
}

<?php
namespace User\ServiceFactory\Model;

use Psr\Container\ContainerInterface;
use User\Model\UserTypeModule;
use User\Model\UserTypeModuleTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class UserTypeModuleTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UserTypeModule());

        $tableGateway=  new TableGateway(
            'user_types_modules',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new UserTypeModuleTable($tableGateway);
    }
}

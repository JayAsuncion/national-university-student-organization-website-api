<?php
namespace User\ServiceFactory\Model;

use Psr\Container\ContainerInterface;
use User\Model\UserCollege;
use User\Model\UserCollegeTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class UserCollegeTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UserCollege());

        $tableGateway = new TableGateway(
            'users_colleges',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new UserCollegeTable($tableGateway);
    }
}

<?php
namespace User\ServiceFactory\Model;

use Psr\Container\ContainerInterface;
use User\Model\UserType;
use User\Model\UserTypeTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class UserTypeTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UserType());

        $tableGateway = new TableGateway(
            'user_types',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new UserTypeTable($tableGateway);
    }
}

<?php
namespace User\ServiceFactory\Model;

use Psr\Container\ContainerInterface;
use User\Model\UserInfoTable;
use User\Model\UserType;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class UserInfoTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UserType());

        $tableGateway = new TableGateway(
            'users_info',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new UserInfoTable($tableGateway);
    }
}

<?php
namespace User;

use User\Controller\UserCredentialController;
use User\Controller\UserTypeController;
use User\Model\UserCollegeTable;
use User\Model\UserCredentialTable;
use User\Model\UserInfoTable;
use User\Model\UserTypeModuleCollegeTable;
use User\Model\UserTypeModuleTable;
use User\Model\UserTypeTable;
use User\ServiceFactory\Controller\UserCredentialControllerFactory;
use User\ServiceFactory\Controller\UserTypeControllerFactory;
use User\ServiceFactory\Model\UserCollegeTableFactory;
use User\ServiceFactory\Model\UserCredentialTableFactory;
use User\ServiceFactory\Model\UserInfoTableFactory;
use User\ServiceFactory\Model\UserTypeModuleCollegeTableFactory;
use User\ServiceFactory\Model\UserTypeModuleTableFactory;
use User\ServiceFactory\Model\UserTypeTableFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => array(
        'routes' => array (
            'users' => array(
                'type' => Segment::class,
                'options' => array(
                    'route' =>'/users[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => UserCredentialController::class
                    )
                )
            ),
            'user-types' => array(
                'type' => Segment::class,
                'options' => array(
                    'route' =>'/user-types[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => UserTypeController::class
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'factories' => array(
            UserCredentialController::class => UserCredentialControllerFactory::class,
            UserTypeController::class => UserTypeControllerFactory::class
        )
    ),
    'service_manager' => array(
        'factories' => array(
            UserCredentialTable::class => UserCredentialTableFactory::class,
            UserCollegeTable::class => UserCollegeTableFactory::class,
            UserInfoTable::class => UserInfoTableFactory::class,
            UserTypeModuleTable::class => UserTypeModuleTableFactory::class,
            UserTypeModuleCollegeTable::class => UserTypeModuleCollegeTableFactory::class,
            UserTypeTable::class => UserTypeTableFactory::class
        ),
        'invokables' => array(

        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);

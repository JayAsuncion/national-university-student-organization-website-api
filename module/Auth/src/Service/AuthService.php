<?php
namespace Auth\Service;

class AuthService
{
    private $tokenService;

    public function __construct(
        TokenService $tokenService
    ) {
        $this->tokenService = $tokenService;
    }

    public function getUserCredentialIDFromAuthHeader($authHeader)
    {
        return $this->tokenService->getValueInAccessToken($authHeader, ['user', 'user_credential_id']);
    }

    public function getUserCredentialNameFromAuthHeader($authHeader)
    {
        return $this->tokenService->getValueInAccessToken($authHeader, ['user', 'user_credential_name']);
    }

    public function getCollegeIDFromAuthHeader($authHeader)
    {
        return $this->tokenService->getValueInAccessToken($authHeader, ['user', 'college_id']);
    }
}

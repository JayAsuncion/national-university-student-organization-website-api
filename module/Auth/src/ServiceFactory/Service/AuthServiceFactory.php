<?php
namespace Auth\ServiceFactory\Service;


use Auth\Service\AuthService;
use Auth\Service\TokenService;
use Psr\Container\ContainerInterface;

class AuthServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $tokenService = $container->get(TokenService::class);

        return new AuthService(
            $tokenService
        );
    }
}

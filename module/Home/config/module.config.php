<?php
namespace Home;

use Home\Controller\HomeCarouselItemController;
use Home\Model\HomeCarouselItemTable;
use Home\ServiceFactory\Controller\HomeCarouselItemControllerFactory;
use Home\ServiceFactory\Model\HomeCarouselItemTableFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => array(
        'routes' => array (
            'home-carousel-items' => array(
                'type' => Segment::class,
                'options' => array(
                    'route' =>'/home-carousel-items[/:id]',
                    'defaults' => array(
                        'controller' => HomeCarouselItemController::class
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'factories' => array(
            HomeCarouselItemController::class => HomeCarouselItemControllerFactory::class
        )
    ),
    'service_manager' => array(
        'factories' => array(
            HomeCarouselItemTable::class => HomeCarouselItemTableFactory::class
        ),
        'invokables' => array(

        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);

<?php
namespace Home\Model;

use Zend\Db\TableGateway\TableGateway;

class HomeCarouselItemTable {
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function insertHomeCarouselItem($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function loadHomeCarouselItemList()
    {
        return $this->tableGateway->select(['delete_flag' => 'n']);
    }

    public function updateHomeCarouselItem($carouselItemID, $data)
    {
        $update = $this->tableGateway->getSql()->update();
        $update->set($data);
        $update->where(['carousel_item_id' => $carouselItemID]);
        return $this->tableGateway->updateWith($update);
    }
}

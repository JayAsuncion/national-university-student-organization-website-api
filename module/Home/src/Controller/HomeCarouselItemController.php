<?php
namespace Home\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Home\Model\HomeCarouselItemTable;
use Zend\View\Model\JsonModel;

class HomeCarouselItemController extends AppAbstractRestfulController
{
    protected $homeCarouselItemTable;

    public function __construct(
        AuthService $authService,
        HomeCarouselItemTable $homeCarouselItemTable
    ) {
        parent::__construct($authService);
        $this->homeCarouselItemTable = $homeCarouselItemTable;
    }

    public function create($data)
    {
        $homeCarouselItemData = [];
        if (!empty($data['label'])) $homeCarouselItemData['label'] = $data['label'];
        if (!empty($data['url'])) $homeCarouselItemData['url'] = json_encode($data['url']);

        $homeCarouselItemID = $this->homeCarouselItemTable->insertHomeCarouselItem($homeCarouselItemData);

        return new JsonModel([
            'success' => true,
            'data' => [
                'carousel_item_id' => $homeCarouselItemID
            ]
        ]);
    }

    public function getList()
    {
        $homeCarouselItemListResultSet = $this->homeCarouselItemTable->loadHomeCarouselItemList();
        $homeCarouselItemList = iterator_to_array($homeCarouselItemListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'home_carousel_item_list' => $homeCarouselItemList
            ]
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['delete_flag'])) {
            $homeCarouselItemData = ['delete_flag' => 'y'];
            $affectedRow = $this->homeCarouselItemTable->updateHomeCarouselItem($id, $homeCarouselItemData);

            return new JsonModel([
                'success' => true,
                'message' => 'Banner #' . $id . ' delete successful'
            ]);
        } else {
            $homeCarouselItemData = [];

            if (!empty($data['label'])) $homeCarouselItemData['label'] = $data['label'];
            if (!empty($data['url'])) $homeCarouselItemData['url'] = json_encode($data['url']);

            $homeCarouselItemID = $id;
            $affectedRow = $this->homeCarouselItemTable->updateHomeCarouselItem($id, $homeCarouselItemData);

            if (empty($affectedRow)) {
                return $this->resourceNotFoundError();
            }

            return new JsonModel([
                'success' => true,
                'data' => [
                    'carousel_item_id' => $homeCarouselItemID
                ]
            ]);
        }
    }
}

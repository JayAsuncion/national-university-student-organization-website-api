<?php
namespace Home\ServiceFactory\Model;

use Home\Model\HomeCarouselItem;
use Home\Model\HomeCarouselItemTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class HomeCarouselItemTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new HomeCarouselItem());

        $tableGateway = new TableGateway(
            'home_carousel_items',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new HomeCarouselItemTable($tableGateway);
    }
}

<?php
namespace Home\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Home\Controller\HomeCarouselItemController;
use Home\Model\HomeCarouselItemTable;
use Psr\Container\ContainerInterface;

class HomeCarouselItemControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $homeCarouselItemTable = $container->get(HomeCarouselItemTable::class);

        return new HomeCarouselItemController(
            $authService, $homeCarouselItemTable
        );
    }
}

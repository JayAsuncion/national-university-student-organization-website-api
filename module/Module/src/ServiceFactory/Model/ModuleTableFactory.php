<?php
namespace Module\ServiceFactory\Model;

use Module\Model\Module;
use Module\Model\ModuleTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class ModuleTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Module());

        $tableGateway = new TableGateway(
            'modules',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new ModuleTable($tableGateway);
    }
}

<?php
namespace Module\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Module\Controller\ModuleController;
use Module\Model\ModuleTable;
use Psr\Container\ContainerInterface;

class ModuleControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $moduleTable = $container->get(ModuleTable::class);

        return new ModuleController(
            $authService,
            $moduleTable
        );
    }
}

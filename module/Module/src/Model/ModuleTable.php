<?php
namespace Module\Model;

use Zend\Db\TableGateway\TableGateway;

class ModuleTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getModuleList()
    {
        $select = $this->tableGateway->getSql()->select();
        return $this->tableGateway->selectWith($select);
    }

    public function getModule($moduleCode)
    {
        return $this->tableGateway->select(['module_code' => $moduleCode]);
    }

    public function insertModule($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateModule($moduleCode, $data)
    {
        return $this->tableGateway->update($data, ['module_code' => $moduleCode]);
    }
}

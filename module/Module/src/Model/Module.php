<?php
namespace Module\Model;

class Module
{
    private $module_code;
    private $module_label;

    public function exchangeArray($data)
    {
        $this->module_code = !empty($data['module_code']) ? $data['module_code'] : '';
        $this->module_label = !empty($data['module_label']) ? $data['module_label'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

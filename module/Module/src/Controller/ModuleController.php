<?php
namespace Module\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Module\Model\ModuleTable;
use Zend\View\Model\JsonModel;

class ModuleController extends AppAbstractRestfulController
{
    protected $moduleTable;

    public function __construct(
        AuthService $authService,
        ModuleTable $moduleTable
    ) {
        parent::__construct($authService);
        $this->moduleTable = $moduleTable;
    }

    public function getList()
    {
        $moduleListResultSet = $this->moduleTable->getModuleList();
        $moduleList = iterator_to_array($moduleListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'module_list' => $moduleList
            ]
        ]);
    }
}

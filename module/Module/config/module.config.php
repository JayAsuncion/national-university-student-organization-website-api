<?php
namespace Module;

use Module\Controller\ModuleController;
use Module\Model\ModuleTable;
use Module\ServiceFactory\Controller\ModuleControllerFactory;
use Module\ServiceFactory\Model\ModuleTableFactory;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'modules' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/modules[/:id]',
                    'defaults' => [
                        'controller' => ModuleController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            ModuleController::class => ModuleControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            ModuleTable::class => ModuleTableFactory::class
        ],
        'invokables' => [
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
];

<?php
namespace EventCategory;

use EventCategory\Controller\EventCategoryController;
use EventCategory\Model\EventCategoryTable;
use EventCategory\ServiceFactory\Controller\EventCategoryControllerFactory;
use EventCategory\ServiceFactory\Model\EventCategoryTableFactory;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'category' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/event-category[/:id]',
                    'defaults' => [
                        'controller' => EventCategoryController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            EventCategoryController::class => EventCategoryControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            EventCategoryTable::class => EventCategoryTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
];

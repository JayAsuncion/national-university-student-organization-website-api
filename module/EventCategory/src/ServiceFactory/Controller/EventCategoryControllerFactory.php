<?php
namespace EventCategory\ServiceFactory\Controller;

use Auth\Service\AuthService;
use EventCategory\Controller\EventCategoryController;
use EventCategory\Model\EventCategoryTable;
use Psr\Container\ContainerInterface;

class EventCategoryControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $categoryTable = $container->get(EventCategoryTable::class);

        return new EventCategoryController(
            $authService,
            $categoryTable
        );
    }
}

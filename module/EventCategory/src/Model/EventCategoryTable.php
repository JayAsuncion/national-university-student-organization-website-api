<?php
namespace EventCategory\Model;

use Zend\Db\TableGateway\TableGateway;

class EventCategoryTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAllCategories()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['event_category_id', 'event_category_name']);
        $select->where(['delete_flag' => 'n']);
        return $this->tableGateway->selectWith($select);
    }

    public function fetchEventCategory($eventCategoryID)
    {
        return $this->tableGateway->select(['event_category_id' => $eventCategoryID, 'delete_flag' => 'n']);
    }

    public function insertEventCategory($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateEventCategory($eventCategoryID, $data)
    {
        return $this->tableGateway->update($data, ['event_category_id' => $eventCategoryID]);
    }
}

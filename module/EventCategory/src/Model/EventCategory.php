<?php
namespace EventCategory\Model;

class EventCategory
{
    public $event_category_id;
    public $event_category_name;

    public function exchangeArray($data)
    {
        $this->event_category_id = (!empty($data['event_category_id'])) ? $data['event_category_id'] : 0;
        $this->event_category_name = (!empty($data['event_category_name'])) ? $data['event_category_name'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

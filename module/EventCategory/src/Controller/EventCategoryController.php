<?php
namespace EventCategory\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use EventCategory\Model\EventCategoryTable;
use Zend\View\Model\JsonModel;

class EventCategoryController extends AppAbstractRestfulController
{
    private $categoryTable;

    public function __construct(
        AuthService $authService,
        EventCategoryTable $categoryTable
    ) {
        parent::__construct($authService);
        $this->categoryTable = $categoryTable;
    }

    public function getList()
    {
        $categoriesResultSet = $this->categoryTable->fetchAllCategories();
        $categories = iterator_to_array($categoriesResultSet->getDataSource());
        $response = [
            'success' => true,
            'data' => [
                'event_category_list' => $categories
            ]
        ];
        return new JsonModel($response);
    }

    public function get($id)
    {
        $eventCategoryResultSet = $this->categoryTable->fetchEventCategory($id);
        $eventCategory = $eventCategoryResultSet->getDataSource()->current();

        if (empty($eventCategory)) {
            return new JsonModel([
                'success' => false,
                'message' => 'Event Category #' . $id . ' not found.'
            ]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'event_category' => $eventCategory
            ]
        ]);
    }

    public function create($data)
    {
        $eventCategoryData = ['event_category_name' => $data['event_category_name']];
        $eventCategoryID = $this->categoryTable->insertEventCategory($eventCategoryData);

        return new JsonModel([
            'success' => true,
            'data' => [
                'event_category_id' => $eventCategoryID
            ]
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['delete_flag'])) {
            $eventCategoryData = ['delete_flag' => 'y'];
            $this->categoryTable->updateEventCategory($id, $eventCategoryData);

            return new JsonModel([
                'success' => true,
                'message' => 'Event Category #'. $id . ' delete successful.'
            ]);
        } else {
            $eventCategoryData = ['event_category_name' => $data['event_category_name']];
            $this->categoryTable->updateEventCategory($id, $eventCategoryData);

            return new JsonModel([
                'success' => true,
                'data' => [
                    'event_category_id' => $id
                ]
            ]);
        }
    }
}

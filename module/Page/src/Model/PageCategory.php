<?php
namespace Page\Model;

class PageCategory
{
    public $category_code;
    public $category_name;

    public function exchangeArray($data)
    {
        $this->category_code = (!empty($data['category_code'])) ? $data['category_code'] : '';
        $this->category_name = (!empty($data['category_name'])) ? $data['category_name'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

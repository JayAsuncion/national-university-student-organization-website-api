<?php
namespace Page\Model;

use Zend\Db\TableGateway\TableGateway;

class PageTemplateTable
{
    private $tableGateway;

    public function __construct(
        TableGateway $tableGateway
    ) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAllPageTemplates($columns = [], $where = [])
    {
        $select = $this->tableGateway->getSql()->select();

        if (count($columns) > 0) {
            $select->columns($columns);
        }

        if (count($where) > 0) {
            $select->where($where);
        }

        return $this->tableGateway->selectWith($select);
    }
}

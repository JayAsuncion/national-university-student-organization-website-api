<?php
namespace Page\Model;

class PageTemplate {
    public $template_code;
    public $template_name;

    public function exchangeArray($data)
    {
        $this->template_code = (!empty($data['template_code'])) ? $data['template_code'] : '';
        $this->template_name = (!empty($data['template_name'])) ? $data['template_name'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

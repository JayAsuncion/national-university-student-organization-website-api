<?php
namespace Page\Model;

class PageDetail
{
    public $page_id;
    public $page_name;
    public $page_url;
    public $template_code;
    public $page_category_code;

    public function exchangeArray($data)
    {
        $this->page_id = (!empty($data['page_id'])) ? $data['page_id'] : 0;
        $this->page_name = (!empty($data['page_name'])) ? $data['page_name'] : '';
        $this->page_url = (!empty($data['page_url'])) ? $data['page_url'] : '';
        $this->template_code = (!empty($data['template_code'])) ? $data['template_code'] : '';
        $this->page_category_code = (!empty($data['page_category_code'])) ? $data['page_category_code'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

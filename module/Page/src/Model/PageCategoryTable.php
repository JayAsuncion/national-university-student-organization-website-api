<?php
namespace Page\Model;

use Zend\Db\TableGateway\TableGateway;

class PageCategoryTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAllPageCategories($where = [])
    {
        $select = $this->tableGateway->getSql()->select();

        if (count($where) > 0) {
            $select->where($where);
        }

        return $this->tableGateway->selectWith($select);
    }
}

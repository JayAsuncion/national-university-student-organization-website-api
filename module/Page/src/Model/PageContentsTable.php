<?php
namespace Page\Model;

use Zend\Db\TableGateway\TableGateway;

class PageContentsTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchPageContentUsingPageDetailID($pageDetailID) {
        $select = $this->tableGateway->getSql()->select();
        $select->where(['page_detail_id' => $pageDetailID]);
        return $this->tableGateway->selectWith($select);
    }


    public function insertPageContent($data) {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }
}

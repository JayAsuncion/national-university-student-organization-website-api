<?php
namespace Page\Model;

use Zend\Db\TableGateway\TableGateway;

class PageDetailsTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAllPages($where)
    {
        $select = $this->tableGateway->getSql()->select();

        if (!empty($where)) {
            $select->where($where);
        }

        return $this->tableGateway->selectWith($select);
    }

    public function fetchPageDetails($pageID) {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['page_name', 'page_url', 'page_description', 'template_code', 'college_id', 'page_category_code']);
        $select->where(['page_detail_id' => $pageID, 'delete_flag' => 'n']);
        return $this->tableGateway->selectWith($select);
    }

    public function insertPage($data) {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updatePage($pageDetailID, $pageDetailData) {
        $this->tableGateway->update($pageDetailData, ['page_detail_id' => $pageDetailID]);
    }
}

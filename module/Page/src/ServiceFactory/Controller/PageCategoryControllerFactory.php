<?php
namespace Page\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Page\Controller\PageCategoryController;
use Page\Model\PageCategoryTable;
use Psr\Container\ContainerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class PageCategoryControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $pageCategoryTable = $container->get(PageCategoryTable::class);

        return new PageCategoryController(
            $authService,
            $pageCategoryTable
        );
    }
}

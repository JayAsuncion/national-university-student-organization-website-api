<?php
namespace Page\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Page\Controller\PageTemplateController;
use Page\Model\PageTemplateTable;
use Psr\Container\ContainerInterface;

class PageTemplateControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $pageTemplateTable = $container->get(PageTemplateTable::class);

        return new PageTemplateController($authService, $pageTemplateTable);
    }
}

<?php
namespace Page\ServiceFactory\Controller;

use Auth\Service\AuthService;
use FormTemplate\Model\FormTemplateContentsTable;
use Page\Controller\PageController;
use Page\Model\PageContentsTable;
use Page\Model\PageDetailsTable;
use Psr\Container\ContainerInterface;

class PageControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $formTemplateContentsTable = $container->get(FormTemplateContentsTable::class);
        $pageContentsTable = $container->get(PageContentsTable::class);
        $pageDetailsTable = $container->get(PageDetailsTable::class);

        return new PageController(
            $authService,
            $formTemplateContentsTable,
            $pageContentsTable,
            $pageDetailsTable
        );
    }
}

<?php
namespace Page\ServiceFactory\Model;

use Page\Model\PageCategory;
use Page\Model\PageCategoryTable;
use Page\Model\PageDetailsTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class PageCategoryTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new PageCategory());

        $tableGateway = new TableGateway(
            'page_categories',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new PageCategoryTable($tableGateway);
    }
}

<?php
namespace Page\ServiceFactory\Model;

use Page\Model\PageDetail;
use Page\Model\PageDetailsTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class PageDetailsTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new PageDetail());

        $tableGateway = new TableGateway(
            'page_details',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new PageDetailsTable($tableGateway);
    }
}

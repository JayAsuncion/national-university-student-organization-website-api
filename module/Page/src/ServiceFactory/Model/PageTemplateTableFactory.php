<?php
namespace Page\ServiceFactory\Model;

use Page\Model\PageTemplate;
use Page\Model\PageTemplateTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class PageTemplateTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new PageTemplate());

        $tableGateway = new TableGateway(
            'page_templates',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new PageTemplateTable($tableGateway);
    }
}

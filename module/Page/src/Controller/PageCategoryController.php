<?php
namespace Page\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Page\Model\PageCategoryTable;
use Zend\View\Model\JsonModel;

class PageCategoryController extends AppAbstractRestfulController
{
    private $pageCategoryTable;

    public function __construct(
        AuthService $authService,
        PageCategoryTable $pageCategoryTable
    ) {
        parent::__construct($authService);
        $this->pageCategoryTable = $pageCategoryTable;
    }

    public function getList()
    {
        $queryParams = $this->params()->fromQuery();
        $smsFlag = !empty($queryParams['sms_flag']) ? $queryParams['sms_flag'] : 'y';
        $pageCategoriesResultSet = $this->pageCategoryTable->fetchAllPageCategories(['sms_flag' => $smsFlag]);
        $pageCategories = iterator_to_array($pageCategoriesResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'page_categories' => $pageCategories
            ]
        ]);
    }
}

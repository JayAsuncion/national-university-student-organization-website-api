<?php
namespace Page\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use FormTemplate\Model\FormTemplateContentsTable;
use Page\Model\PageContentsTable;
use Page\Model\PageDetailsTable;
use Zend\View\Model\JsonModel;

class PageController extends AppAbstractRestfulController
{
    private $formTemplateContentsTable;
    private $pageContentsTable;
    private $pageDetailsTable;

    public function __construct(
        AuthService $authService,
        FormTemplateContentsTable $formTemplateContentsTable,
        PageContentsTable $pageContentsTable,
        PageDetailsTable $pageDetailsTable
    ) {
        parent::__construct($authService);
        $this->formTemplateContentsTable = $formTemplateContentsTable;
        $this->pageContentsTable = $pageContentsTable;
        $this->pageDetailsTable = $pageDetailsTable;
    }

    public function getList()
    {
        $collegeID = $this->params()->fromQuery('college_id');
        $pageCategoryCode = $this->params()->fromQuery('page_category_code');

        if (empty($pageCategoryCode)) {
            return $this->resourceNotFoundError('Page Category Code is required.');
        }

        $pagesResultSet = $this->pageDetailsTable->fetchAllPages([
            'page_category_code' => $pageCategoryCode,
            'college_id' => $collegeID,
            'delete_flag' => 'n'
        ]);
        $pages = iterator_to_array($pagesResultSet->getDataSource());
        return new JsonModel([
            'success' => true,
            'data' => [
                'pageList' => $pages
            ]
        ]);
    }

    public function get($id)
    {
        $pageDetailsResultSet = $this->pageDetailsTable->fetchPageDetails($id);
        $pageDetails = $pageDetailsResultSet->getDataSource()->current();

        if (empty($pageDetails)) {
            return new JsonModel([
                'success' => false
            ]);
        }

        $pageContentResultSet = $this->pageContentsTable->fetchPageContentUsingPageDetailID($id);
        $pageContent = $pageContentResultSet->getDataSource()->current();

        $formTemplateContentsResultSet = $this->formTemplateContentsTable->fetchFormTemplateContent(
            $pageContent['form_template_content_id']);
        $formTemplateContent = $formTemplateContentsResultSet->getDataSource()->current();
        $jsonContent = json_decode($formTemplateContent['json_content']);

        return new JsonModel([
            'success' => true,
            'data' => [
                'page_details' => $pageDetails,
                'page_content' => $pageContent,
                'form_template_content_code' => $formTemplateContent['template_code'],
                'form_template_content' => $jsonContent
            ]
        ]);
    }

    public function create($data)
    {
        $pageDetails = $data['page_details'];
        $pageDetails['user_credential_id'] = $this->getUserCredentialIDFromAuthHeader();
        $pageDetails['user_credential_name'] = $this->getUserCredentialNameFromAuthHeader();
        $pageDetails['college_id'] = $this->getCollegeIDFromAuthHeader();
        $pageDetails['date_created'] = date('Y-m-d H:i:s');

        $pageDetailsID = $this->pageDetailsTable->insertPage($pageDetails);

        // TODO: Update post_max_size && upload_max_filesize in php.ini

        $jsonContent = [
            'event_name'        => $data['page_content']['event_name'],
            'event_date'        => $data['page_content']['event_date'],
            'event_category'    => $data['page_content']['event_category'],
            'event_images'      => $data['page_content']['event_images'],
            'event_content'     => $data['page_content']['event_content'],
            'sponsors_partners'     => $data['page_content']['sponsors_partners']
        ];

        if ($pageDetails['page_category_code'] === 'CONGRESS_SESSION_PAGE') {
            $formTemplateCode =  'CONGRESS_SESSION_FORM_TPL';
        }

        if ($pageDetails['page_category_code'] === 'EVENT_PAGE') {
            $formTemplateCode =  'EVENT';
        }

        $formTemplateContent = [
            'json_content' => json_encode($jsonContent),
            'template_code' => $formTemplateCode,
            'event_date' => date("Y-m-d H:i:s", strtotime($data['page_content']['event_date'])),
            'event_category' => !empty($data['page_content']['event_category']) ? $data['page_content']['event_category'] : 0
        ];
        $formTemplateContentID = $this->formTemplateContentsTable->insertFormTemplateContent($formTemplateContent);

        $pageContent = [
            'form_template_content_id' => $formTemplateContentID,
            'page_detail_id' => $pageDetailsID
        ];
        $pageContentID = $this->pageContentsTable->insertPageContent($pageContent);

        return new JsonModel([
            'success' => true,
            'data' => [
                'page_detail_id' => $pageDetailsID,
            ]
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['delete_flag']) && $data['delete_flag'] == 'y') {
            $this->deletePage($id);

            return new JsonModel([
                'success' => true,
                'message' => 'Delete Page #' . $id . ' was successful.'
            ]);
        } else {
            $pageDetailID = $id;
            $pageDetails = $data['page_details'];
            $pageContent = $data['page_content'];
            $formTemplateContent = $data['form_template_content'];

            $this->pageDetailsTable->updatePage($pageDetailID, $pageDetails);

            switch ($pageDetails['template_code']) {
                case 'ABOUT_US_TPL':
                    $jsonContent = [
                        'vision'        => $formTemplateContent['vision'],
                        'mission'        => $formTemplateContent['mission'],
                        'organizational_chart'    => $formTemplateContent['organizational_chart'],
                        'officers'      => $formTemplateContent['officers']
                    ];

                    $formTemplateContent = [
                        'json_content' => json_encode($jsonContent),
                        'template_code' => 'ABOUT_US_FORM_TPL'
                    ];
                    break;
                case 'CONTACT_US_TPL':
                    $jsonContent = [
                        'email1'        => $formTemplateContent['email1'],
                        'email2'        => $formTemplateContent['email2'],
                        'facebook'    => $formTemplateContent['facebook'],
                        'twitter'      => $formTemplateContent['twitter'],
                        'instagram'      => $formTemplateContent['instagram'],
                        'address1'      => $formTemplateContent['address1'],
                        'address2'      => $formTemplateContent['address2']
                    ];

                    $formTemplateContent = [
                        'json_content' => json_encode($jsonContent),
                        'template_code' => 'CONTACT_US_FORM_TPL'
                    ];
                    break;
                case 'EVENT':
                    $jsonContent = [
                        'event_name'        => $formTemplateContent['event_name'],
                        'event_date'        => $formTemplateContent['event_date'],
                        'event_category'    => $formTemplateContent['event_category'],
                        'event_images'      => $formTemplateContent['event_images'],
                        'event_content'     => $formTemplateContent['event_content'],
                        'sponsors_partners'     => $formTemplateContent['sponsors_partners']
                    ];

                    $formTemplateContent = [
                        'json_content' => json_encode($jsonContent),
                        'template_code' => 'EVENT',
                        'event_date' => date("Y-m-d H:i:s", strtotime($formTemplateContent['event_date'])),
                        'event_category' => $formTemplateContent['event_category']
                    ];
                    break;
                case 'CONGRESS_SESSION_TPL':
                    $jsonContent = [
                        'event_name'        => $formTemplateContent['event_name'],
                        'event_date'        => $formTemplateContent['event_date'],
                        'event_images'      => $formTemplateContent['event_images'],
                        'event_content'     => $formTemplateContent['event_content']
                    ];

                    $formTemplateContent = [
                        'json_content' => json_encode($jsonContent),
                        'template_code' => 'CONGRESS_SESSION_FORM_TPL',
                        'event_date' => date("Y-m-d H:i:s", strtotime($formTemplateContent['event_date'])),
                        'event_category' => $formTemplateContent['event_category']
                    ];
                    break;
            }

            $formTemplateContentID = $pageContent['form_template_content_id'];
            $this->formTemplateContentsTable->updateFormTemplateContent($formTemplateContentID, $formTemplateContent);

            return new JsonModel([
                'success' => true,
                'data' => [
                    'page_detail_id' => $pageDetailID
                ]
            ]);
        }

        return new JsonModel(['success' => false, 'message' => 'Neither update nor delete operation was not performed.']);
    }

    private function deletePage($pageDetailID) {
        $data = [
            'delete_flag' => 'y'
        ];
        $this->pageDetailsTable->updatePage($pageDetailID, $data);

        return true;
    }
}

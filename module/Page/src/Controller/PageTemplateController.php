<?php
namespace Page\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Page\Model\PageTemplateTable;
use Zend\View\Model\JsonModel;

class PageTemplateController extends AppAbstractRestfulController
{
    private $pageTemplateTable;

    public function __construct(
        AuthService $authService,
        PageTemplateTable $pageTemplateTable
    ) {
        parent::__construct($authService);
        $this->pageTemplateTable = $pageTemplateTable;
    }

    public function getList()
    {
        $queryParams = $this->params()->fromQuery();
        $typeParam = !empty($queryParams['type']) ? $queryParams['type'] : '';
        $smsFlag = !empty($queryParams['sms_flag']) ? $queryParams['sms_flag'] : 'y';
        $pageTemplateResultSet = '';

        switch ($typeParam) {
            case 'dropdown':
                $pageTemplateResultSet = $this->pageTemplateTable->fetchAllPageTemplates(
                    ['template_code', 'template_name'],
                    ['sms_flag' => $smsFlag]
                );
                break;
            default:
                $pageTemplateResultSet = $this->pageTemplateTable->fetchAllPageTemplates([], ['sms_flag' => $smsFlag]);
                break;
        }

        $pageTemplateList = iterator_to_array($pageTemplateResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'page_template_list' => $pageTemplateList
            ]
        ]);
    }
}

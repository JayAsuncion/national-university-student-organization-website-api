<?php
namespace Page;

use Page\Controller\PageCategoryController;
use Page\Controller\PageController;
use Page\Controller\PageTemplateController;
use Page\Model\PageCategoryTable;
use Page\Model\PageContentsTable;
use Page\Model\PageDetailsTable;
use Page\Model\PageTemplateTable;
use Page\ServiceFactory\Controller\PageCategoryControllerFactory;
use Page\ServiceFactory\Controller\PageControllerFactory;
use Page\ServiceFactory\Controller\PageTemplateControllerFactory;
use Page\ServiceFactory\Model\PageCategoryTableFactory;
use Page\ServiceFactory\Model\PageContentsTableFactory;
use Page\ServiceFactory\Model\PageDetailsTableFactory;
use Page\ServiceFactory\Model\PageTemplateTableFactory;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'pages' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/pages[/:id]',
                    'defaults' => [
                        'controller' => PageController::class
                    ]
                ]
            ],
            'page_categories' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/page-categories[/:id]',
                    'defaults' => [
                        'controller' => PageCategoryController::class
                    ]
                ],
            ],
            'page_templates' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/page-templates[/:id]',
                    'defaults' => [
                        'controller' => PageTemplateController::class
                    ]
                ],
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            PageController::class => PageControllerFactory::class,
            PageCategoryController::class => PageCategoryControllerFactory::class,
            PageTemplateController::class => PageTemplateControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            PageDetailsTable::class => PageDetailsTableFactory::class,
            PageContentsTable::class => PageContentsTableFactory::class,
            PageCategoryTable::class => PageCategoryTableFactory::class,
            PageTemplateTable::class => PageTemplateTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
];

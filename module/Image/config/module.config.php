<?php
namespace Image;

use Image\Model\FilesTable;
use Image\Model\ImagesTable;
use Image\ServiceFactory\Controller\ImageControllerFactory;
use Image\Controller\ImageController;
use Image\ServiceFactory\Model\FilesTableFactory;
use Image\ServiceFactory\Model\ImagesTableFactory;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'images' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/images[/:id]',
                    'defaults' => [
                        'controller' => ImageController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            ImageController::class => ImageControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            FilesTable::class => FilesTableFactory::class,
            ImagesTable::class => ImagesTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
];

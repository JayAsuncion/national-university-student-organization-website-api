<?php
namespace Image\ServiceFactory\Model;

use Image\Model\Image;
use Image\Model\ImagesTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class ImagesTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Image());
        $tableGateway = new TableGateway(
            'images',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new ImagesTable($tableGateway);
    }
}

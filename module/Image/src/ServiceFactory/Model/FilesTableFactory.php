<?php
namespace Image\ServiceFactory\Model;

use Image\Model\File;
use Image\Model\FilesTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class FilesTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new File());
        $tableGateway = new TableGateway(
            'files',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new FilesTable($tableGateway);
    }
}

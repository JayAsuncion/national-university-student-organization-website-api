<?php
namespace Image\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Aws\S3\S3Client;
use Image\Controller\ImageController;
use Image\Model\FilesTable;
use Image\Model\ImagesTable;
use Psr\Container\ContainerInterface;

class ImageControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $filesTable = $container->get(FilesTable::class);
        $imagesTable = $container->get(ImagesTable::class);
        $s3Client = new S3Client([
            'version' => 'latest',
            'region' => 'ap-southeast-1',
            'credentials' => [
                'key' => 'AKIA4WXQ4UINU7QKSJGP',
                'secret' => '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'
            ]
        ]);

        return new ImageController(
            $authService,
            $filesTable,
            $imagesTable,
            $s3Client
        );
    }
}

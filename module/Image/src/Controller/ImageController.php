<?php
namespace Image\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Image\Model\FilesTable;
use Image\Model\ImagesTable;
use Zend\View\Model\JsonModel;

class ImageController extends AppAbstractRestfulController
{
    private $filesTable;
    private $imagesTable;
    private $s3Client;

    public function __construct(
        AuthService $authService,
        FilesTable $filesTable,
        ImagesTable $imagesTable,
        S3Client $s3Client
    ) {
        parent::__construct($authService);
        $this->filesTable = $filesTable;
        $this->imagesTable = $imagesTable;
        $this->s3Client = $s3Client;
    }

    public function create($data)
    {
        $request = $this->getRequest();
        $params = $request->getQuery();
        $files = $request->getFiles();
        $uploader = $params['uploader'];
        $uploadType = (!empty($data['upload_type'])) ? $data['upload_type']: 'images';
        $folder = (!empty($data['folder'])) ? $data['folder']: '';

        // Insert Images and Move Image
        $uploadDirectory = dirname(dirname($_SERVER['DOCUMENT_ROOT'])) . '/images/';
        $currentDateTime = date('Y-m-d H:i:s');
        $responseData = [];
        $imagesResponseData = [];

        foreach ($files as $file) {
            $uploadData = ['url' => 'temp_url', 'date_uploaded' => $currentDateTime];
            $ID = $this->insertTableRecord($uploadData, $uploadType);
            $fileName = pathinfo($file['name'], PATHINFO_FILENAME);
            $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);
            $newFileName =  $fileName . '_' . $ID . '.' .  $fileExtension;
            //$uploadPath = $uploadDirectory . $newFileName;
            //move_uploaded_file($file['tmp_name'], $uploadPath);

            $bucket = 'nusg';
            $keyname = $folder . '/' . $newFileName;
            $cacheControl = ['1_YEAR' => 1536000];

            try {
                $request = $this->s3Client->putObject([
                    'Bucket' => $bucket,
                    'Key' => $keyname,
                    'SourceFile' => $file['tmp_name'],
                    'ACL' => 'public-read',
                    'ContentType' => $file['type'],
                    'CacheControl' => 'max-age=' . $cacheControl['1_YEAR']
                ]);
                $uploadPath = $request['ObjectURL'];
            } catch (S3Exception $e) {
                $errorMessage =  $e->getMessage() . PHP_EOL;
                return new JsonModel([
                    'success' => false,
                    'error' => [
                        'message' => $errorMessage
                    ]
                ]);
            }

            $this->updateRecordUploadPath($ID, $uploadPath, $uploadType);

            if ($uploader == 'CK_EDITOR') {
                $imagesResponseData = $uploadPath;
            } else if ($uploadType === 'resolutions') {
                array_push($imagesResponseData, ['file_id' => $ID, 'url' => $uploadPath]);
            } else if ($uploadType === 'college_courses_offered') {
                array_push($imagesResponseData, ['file_id' => $ID, 'url' => $uploadPath]);
            } else {
                array_push($imagesResponseData, ['image_id' => $ID, 'url' => $uploadPath]);
            }
        }

        if ($uploadType === 'resolutions') {
            $responseData = [
                'files' => $imagesResponseData
            ];
        } else if($uploadType === 'college_courses_offered') {
            $responseData = [
                'files' => $imagesResponseData
            ];
        } else {
            $responseData = [
                'images' => $imagesResponseData
            ];
        }

        return new JsonModel([
            'success' => true,
            'url' => $imagesResponseData,
            'data' => $responseData
        ]);
    }

    private function insertTableRecord($uploadData, $uploadType = 'images')
    {
        $ID = 0;

        switch ($uploadType) {
            case 'images':
                $ID = $this->imagesTable->insertImage($uploadData);
                break;
            case 'resolutions':
                $uploadData['type'] = $uploadType;
                $ID = $this->filesTable->insertFile($uploadData);
                break;
            default:
                $ID = $this->imagesTable->insertImage($uploadData);
                break;
        }

        return $ID;
    }

    private function updateRecordUploadPath($ID, $uploadPath, $uploadType = 'images')
    {
        switch ($uploadType) {
            case 'images':
                $this->imagesTable->updateImageUrl($ID, $uploadPath);
                break;
            case 'resolutions':
                $this->filesTable->updateFileURL($ID, $uploadPath);
                break;
            default:
                $this->imagesTable->updateImageUrl($ID, $uploadPath);
                break;
        }

        return;
    }
}

<?php
namespace Image\Model;

class File
{
    public $file_id;
    public $url;
    public $type;
    public $date_uploaded;

    public function exchangeArray($data)
    {
        $this->image_id = (!empty($data['image_id'])) ? $data['image_id'] : 0;
        $this->url = (!empty($data['url'])) ? $data['url'] : '';
        $this->type = (!empty($data['type'])) ? $data['type'] : '';
        $this->date_uploaded = (!empty($data['date_uploaded'])) ? $data['date_uploaded'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

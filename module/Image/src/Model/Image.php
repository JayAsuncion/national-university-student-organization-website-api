<?php
namespace Image\Model;

class Image
{
    public $image_id;
    public $url;
    public $date_uploaded;

    public function exchangeArray($data)
    {
        $this->image_id = (!empty($data['image_id'])) ? $data['image_id'] : 0;
        $this->url = (!empty($data['url'])) ? $data['url'] : '';
        $this->date_uploaded = (!empty($data['date_uploaded'])) ? $data['date_uploaded'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

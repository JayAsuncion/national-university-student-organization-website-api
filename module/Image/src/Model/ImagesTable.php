<?php
namespace Image\Model;

use Zend\Db\TableGateway\TableGateway;

class ImagesTable
{
    private $tableGateway;

    public function __construct(
        TableGateway $tableGateway
    ) {
        $this->tableGateway = $tableGateway;
    }

    public function insertImage($imagesData) {
        $insert = $this->tableGateway->getSql()->insert();
        $insert->values($imagesData);
        $this->tableGateway->insertWith($insert);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateImageUrl($imageID, $imageUrl) {
        $update = $this->tableGateway->getSql()->update();
        $update->set(['url' => $imageUrl]);
        $update->where(['image_id', $imageID]);
        $this->tableGateway->updateWith($update);
    }
}

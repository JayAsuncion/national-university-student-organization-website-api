<?php
namespace Image\Model;

use Zend\Db\TableGateway\TableGateway;

class FilesTable
{
    private $tableGateway;

    public function __construct(
        TableGateway $tableGateway
    ) {
        $this->tableGateway = $tableGateway;
    }

    public function insertFile($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateFileURL($id, $url)
    {
        return $this->tableGateway->update(['url' => $url], ['file_id' => $id]);
    }
}

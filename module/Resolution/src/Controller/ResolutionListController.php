<?php
namespace Resolution\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Resolution\Model\ResolutionTable;
use Zend\View\Model\JsonModel;

class ResolutionListController extends AppAbstractRestfulController
{
    private $resolutionTable;

    public function __construct(
        AuthService $authService,
        ResolutionTable $resolutionTable
    ) {
        parent::__construct($authService);
        $this->resolutionTable = $resolutionTable;
    }

    public function getList()
    {
        $resolutionListResultSet = $this->resolutionTable->getResolutionList();
        $resolutionList = iterator_to_array($resolutionListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'resolution_list' => $resolutionList
            ]
        ]);
    }

    public function get($id)
    {
        $resolutionResultSet = $this->resolutionTable->getResolution($id);
        $resolution = $resolutionResultSet->getDataSource()->current();

        if (empty($resolution)) {
            return new JsonModel([
                'success' => false,
                'message' => 'Resolution #' . $id . ' not found.'
            ]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'resolution' => $resolution
            ]
        ]);
    }

    public function create($data)
    {
        $resolutionData = [
            'resolution_title' => !empty($data['resolution_title']) ? $data['resolution_title'] : '',
            'resolution_description' => !empty($data['resolution_description']) ? $data['resolution_description'] : '',
            'resolution_url' => !empty($data['resolution_url']) ? json_encode($data['resolution_url']) : '',
            'uploaded_by' => $this->getUserCredentialNameFromAuthHeader(),
            'uploaded_at' => date('Y-m-d H:i:s')
        ];

        $resolutionID = $this->resolutionTable->insertResolution($resolutionData);

        return new JsonModel([
            'success' => true,
            'data' => [
                'resolution_id' => $resolutionID
            ]
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['delete_flag'])) {
            $resolutionData = ['delete_flag' => 'y'];
            $this->resolutionTable->updateResolution($id, $resolutionData);

            return new JsonModel([
                'success' => true,
                'message' => 'Resolution #' . $id . ' delete successful'
            ]);
        } else {
            $resolutionData = [
                'resolution_title' => !empty($data['resolution_title']) ? $data['resolution_title'] : '',
                'resolution_description' => !empty($data['resolution_description']) ? $data['resolution_description'] : '',
                'resolution_url' => !empty($data['resolution_url']) ? json_encode($data['resolution_url']) : '',
            ];

            $this->resolutionTable->updateResolution($id, $resolutionData);

            return new JsonModel([
                'success' => true,
                'data' => [
                    'resolution_id' => $id
                ]
            ]);
        }
    }
}
